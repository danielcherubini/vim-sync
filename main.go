package main

import (
	"fmt"

	"github.com/danielcherubini/vim-sync/utils"
)

func main() {
	dir := utils.GetDirectory()
	fmt.Println(dir)
	conf, err := utils.GetConfig("/Users/danielcherubini/Coding/Go/github.com/danielcherubini/vim-sync/config.yaml")
	if err != nil {
		utils.HandleError(err, "Error Getting Config")
	}
	fmt.Println(conf)
}
