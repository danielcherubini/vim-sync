// Package models provides ...
package models

// Model has a thing
type Person struct {
	Name string
	Age  int
}

// Config is a struct
type Config struct {
	Type   string
	Github struct {
		Username string `yaml:"username"`
		Repo     string `yaml:"repo"`
	}
}
