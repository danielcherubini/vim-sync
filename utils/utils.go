package utils

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/danielcherubini/vim-sync/models"
	"gopkg.in/yaml.v2"
)

// HandleError takes a error and a reason why
// error is a error
// why is a string
// prints a concat of why + " " + error.Error
// then crashes
func HandleError(e error, why string) {
	fmt.Println(why + " " + e.Error())
	os.Exit(1)
}

// GetDirectory gets the current directory
// returns current directory as string
func GetDirectory() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		HandleError(err, "Error getting current directory")
	}
	return dir
}

// GetConfig gets the config from a directory
// filepath is the path of the file you want to open
// Returns a models.Config and an error
func GetConfig(filepath string) (models.Config, error) {
	dat, err := ioutil.ReadFile(filepath)
	if err != nil {
		HandleError(err, "Couldn't read file at "+filepath)
	}
	fmt.Println(string(dat))
	c := models.Config{}
	err = yaml.Unmarshal([]byte(dat), &c)
	if err != nil {
		return c, err
	}
	return c, nil
}
